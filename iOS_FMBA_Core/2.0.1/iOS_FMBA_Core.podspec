Pod::Spec.new do |s|


s.platform = :ios
s.ios.deployment_target = '10.3'
s.name = "iOS_FMBA_Core"
s.summary = "The latest FMBA Framework"
s.requires_arc = true

# 2
s.version = "2.0.1"

# 3
s.license = { :type => "RESTRICTED", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "SUSH Mobile" => "ios@sushmobile.com" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
s.homepage = "https://github.com/TheCodedSelf/RWPickFlavor"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://amvfortuna@bitbucket.org/amvfortuna/fmbaredesign.git", :tag => s.version }

# 7
s.framework = "UIKit"
s.framework = "MessageUI"
s.framework = "SafariServices"
s.framework = "WebKit"
s.dependency 'KeychainSwift', '~> 10.0'
s.dependency 'RNCryptor', '~> 5.0'
s.dependency 'Alamofire', '~> 4.6'
s.dependency 'SkyFloatingLabelTextField', '~> 3.4'
s.dependency 'ReachabilitySwift', '~> 4.3.0'
s.dependency 'AlamofireNetworkActivityLogger', '~> 2.0'
s.dependency 'UITextView+Placeholder', '~> 1.2.1'
s.dependency 'Validator', '~> 3.0.2'

# 8
s.source_files = "iOS_FMBA_Core/**/*.{swift}"

# 9
s.resources = "iOS_FMBA_Core/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"

# 10
s.swift_version = "4.0"

end
